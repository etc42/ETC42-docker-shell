#!/bin/sh

. ./.env

#---------------------------#
#-------- functions --------#
#---------------------------#

fonction_get_rtrn()
{
  # fonction_get_rtrn:  get return result part
  # params:
  #    $1 - return result string
  #    $2 - number of part to be returned
  # returns:
  #    return result part

    echo `echo $1|cut -d "," -f $2`
}

function_jar_updated_search()
{
  # function_jar_updated_search: get the last jar version
  # returns:
  #    return version number_version

  curl -s ${CESAM_URL} | awk -F">" '/ETC-Jar/{print $7, $10}' > .temp
  sed -Ee 's/<\/a/ /g' .temp | awk -F"  " '{print$2,$1}' > .temp2
  version=$(sort -r .temp2 | head -1 | awk '{print $3}')
  number_version=$(echo ${version:8} | sed 's/\.jar//')
  echo "$version,$number_version"
}

function_delete_image()
{
  # function_delete_image: delete the image
  # params:
  #    $1 - etc42 version image to delete

  echo "- Deleting the etc42-$1 docker image ..."
  old_id=$(docker inspect --format="{{.Id}}" ${IMAGE_ETC42}-$1)
  docker rmi ${old_id} #remove old etc42 image
  echo "=> Deleting the etc42-$1 docker image successfully"

}

function_build_image()
{
  # function_build_image: build the etc42 image

      version=$1

      curl -s ${CESAM_URL} | awk -F">" '/ETC-Jar/{print $7, $10}' > .temp
      sed -Ee 's/<\/a/ /g' .temp | awk -F"  " '{print$2,$1}' > .temp2
      list_jar=$(sort -r .temp2 | awk '{print $3}')
      present=$(echo $list_jar| grep $version)

      if [ $? -eq 1 ]; then

        echo ''
        echo '------------------------------------'
        echo ''
        echo "You must select an existing version"
        echo "   available on the CeSAM server   "
        echo ''
        echo 'To see the available version(s):'
        echo ''
        echo "Please make ./etc42_osx.sh -i OR --infos"
        echo ''
        echo '------------------------------------'
        echo ''
        exit 0

      else

        echo ''
        echo "- Searching for the current etc42-${version} docker image ..."
        id_image=$(docker inspect --format="{{.Id}}" ${IMAGE_ETC42}-${version})

        if [ ${id_image} ]; then

          echo "=> The etc42-${version} docker image already exist"
          echo ''

        else

          #----------------------------------------#
          wget -o .logfile -P ${CONF_REPO_ETC42} ${CESAM_URL}/ETC-Jar-${version}.jar
          mv ${CONF_REPO_ETC42}/ETC-Jar-${version}.jar ${CONF_REPO_ETC42}/etc42-${version}.jar

          #-----------------#
          #build
          echo "- Creating the etc42-${version} docker image in progress ..."
          echo '------------------------------------------------------------------'
          docker build --build-arg username=$USER --build-arg uidval=$UID --build-arg home=/home/$USER --build-arg etc42_version=${version} -t ${IMAGE_ETC42}-${version} ${CONF_REPO_ETC42}
          echo "=> creating the etc42-${version} docker image successfully"
          echo '------------------------------------------------------------------'
          echo ''
          #-----------------#
          rm ${CONF_REPO_ETC42}/etc42-${version}.jar
          rm .logfile

        fi
      fi

      rm .temp
      rm .temp2
}

#---------------------------#
#---------------------------#
#---------------------------#


#-----------------------------------------#
#----------- main program ----------------#
#-----------------------------------------#


case $1 in
    ""|"--select")
      #######################
      # use: ./etc42_osx.sh #
      #######################

      # ------------------------------------------ #
      version_init=$(cat .env | grep ETC42_VERSION)

      if [ ${version_init} == "ETC42_VERSION=?" ]; then

        init='TRUE'

        echo "INITIALISATION VERSION"
        #----------------------------------------#
        #call function function_jar_updated_search

        RESULT=`function_jar_updated_search`
        etc42_last_version=`fonction_get_rtrn $RESULT 1`
        etc42_last_number_version=`fonction_get_rtrn $RESULT 2`
        #----------------------------------------#
        sed -i '' "s/ETC42_VERSION=?/ETC42_VERSION=${etc42_last_number_version}/" .env
        rm .temp
        rm .temp2

      else

        init='FALSE'

      fi
      # ---------------------------------------- #

      if [ ${init} == TRUE ]; then

        version=${etc42_last_number_version}

      else

        if [ "$1" == "--select" ]; then

          # --------------- #
          curl -s ${CESAM_URL} | awk -F">" '/ETC-Jar/{print $7, $10}' > .temp
          sed -Ee 's/<\/a/ /g' .temp | awk -F"  " '{print$2,$1}' > .temp2
          list_jar=$(sort -r .temp2 | awk '{print $3}')
          present=$(echo $list_jar| grep $2)

          if [ $? -eq 1 ]; then

            echo ''
            echo '------------------------------------'
            echo ''
            echo "You must select an existing version"
            echo "   available on the CeSAM server   "
            echo ''
            echo 'To see the available version(s):'
            echo ''
            echo "Please make ./etc42_osx.sh -i OR --infos"
            echo ''
            echo '------------------------------------'
            echo ''
            exit 0
          else

            version=$2

          fi
          # --------------- #

        else

          version=${ETC42_VERSION}

        fi

      fi

      echo ''
      echo '------------------------------------'
      echo ' Universal Exposure Time Calculator '
      echo '               ETC42                '
      echo ''
      echo '- etc42 version: ' ${version}

      #----------------------------------------#
      #call function function_jar_updated_search

      RESULT=`function_jar_updated_search`
      etc42_last_version=`fonction_get_rtrn $RESULT 1`
      etc42_last_number_version=`fonction_get_rtrn $RESULT 2`

      #----------------------------------------#

      if [ ${etc42_last_number_version} != ${version} ]; then

        echo ''
        echo '========================================'
        echo 'YOU ARE NOT USING THE LAST ETC42 VERSION'
        echo '========================================'
        echo ''
        echo '=> A new ETC42 version is available:' ${etc42_last_number_version}
        echo ''
        echo 'COMMAND TO UPDATE ETC42 VERSION:'
        echo ''
        echo '=> ./etc42_osx.sh -u OR --update'
        echo '------------------------------------'
        echo ''
        echo ''

        sleep 5

      else
        #case no update

        echo ''
        echo '------------------------------------'
        echo ''

      fi

      rm .temp
      rm .temp2


      #----------------------------------------#
      #call function function_build_image

      # function_build_image
      if [ ${version} == ${ETC42_VERSION} ]; then

        function_build_image ${ETC42_VERSION}

      else

        function_build_image $version

      fi

      #----------------------------------------#

      #------------------------#
      #lockfile management

      if [ $SAMP_HUB ]; then
        #if the environment variable
        #SAMP_HUB is defined on the host

        SAMP_HUB_LOCATE=$SAMP_HUB

      else

        SAMP_HUB_LOCATE='std-lockurl:file:////home/'$USER'/.samp'

      fi


      #------------------------#

      echo "- Container construction and launch of ${CONTAINER_NAME_ETC42} in progress ... "

      open -a XQuartz
      xhost +
      docker run --name ${CONTAINER_NAME_ETC42} --rm -v /tmp/.X11-unix:/tmp/.X11-unix -v /Users/$USER:/home/$USER --network host -e DISPLAY=host.docker.internal:0 -e USER=$USER -e HOME=$HOME -e SAMP_HUB=${SAMP_HUB_LOCATE} ${IMAGE_ETC42}-${version} java -jar /var/etc42/etc42-${version}.jar
      xhost -

      #------------------------#
      echo "=> End of etc42 program"
      echo ""

      ;;

    "-b"|"--build")
      #####################################
      # use: ./etc42_osx.sh -b OR --build #
      #####################################

      version=$2

      list_jar=$(docker images | awk '/^image_etc42-/{print $1}')
      present=$(echo $list_jar| grep $version)

      if [ $? -eq 1 ]; then

        echo ''
        echo '------------------------------------'
        echo ''
        echo "You must select an existing version"
        echo "    available on the localhost     "
        echo ''
        echo 'To see the available version(s):'
        echo ''
        echo "Please make ./etc42_osx.sh -i OR --infos"
        echo ''
        echo '------------------------------------'
        echo ''
        exit 0

      else

        #------------------------------------#
        #call function function_delete_image

        function_delete_image $2

        #------------------------------------#
        #call function function_build_image

        function_build_image $2

        #------------------------------------#

      fi

      ;;

    "-u"|"--update")
      ######################################
      # use: ./etc42_osx.sh -u OR --update #
      ######################################

      #----------------------------------------#
      #call function function_jar_updated_search

      RESULT=`function_jar_updated_search`
      etc42_last_version=`fonction_get_rtrn $RESULT 1`
      etc42_last_number_version=`fonction_get_rtrn $RESULT 2`

      #----------------------------------------#

      old_etc42_version=${ETC42_VERSION}
      new_etc42_version=${etc42_last_number_version}

      if [ ${new_etc42_version} != ${old_etc42_version} ]; then
        #case update available

        wget -o .logfile -P ${CONF_REPO_ETC42} ${CESAM_URL}/ETC-Jar-${new_etc42_version}.jar
        mv ${CONF_REPO_ETC42}/ETC-Jar-${new_etc42_version}.jar ${CONF_REPO_ETC42}/etc42-${new_etc42_version}.jar

        sed -i "s/${old_etc42_version}/${new_etc42_version}/" .env

        echo "=> An update is available"
        echo "- new update version:" ${new_etc42_version}

        #-----------------------------------#
        #call function function_delete_image

        function_delete_image ${old_etc42_version}

        #-----------------------------------#

        echo "- Creating the etc42-${new_etc42_version} docker image in progress ..."
        docker build --build-arg username=$USER --build-arg uidval=$UID --build-arg home=$HOME --build-arg etc42_version=${new_etc42_version} -t ${IMAGE_ETC42}-${new_etc42_version} ${CONF_REPO_ETC42}
        echo "=> creating the etc42-${new_etc42_version} docker image successfully"

        echo ''
        echo '------------------------------------'
        echo ''
        echo "- ETC42 is up to date"
        echo '- etc42 version: ' ${new_etc42_version}
        echo ''
        echo '------------------------------------'

        rm ${CONF_REPO_ETC42}/etc42-${new_etc42_version}.jar
        rm .logfile

      else
        #case no update

        echo '------------------------------------'
        echo ''
        echo "- ETC42 is already up to date"
        echo '- etc42 version: ' ${ETC42_VERSION}
        echo ''
        echo '------------------------------------'

      fi

      rm .temp
      rm .temp2

      ;;

    "-i"|"--infos")
      #####################################
      # use: ./etc42_osx.sh -i OR --infos #
      #####################################

      curl -s ${CESAM_URL} | awk -F">" '/ETC-Jar/{print $7, $10}' > .temp
      sed -Ee 's/<\/a/ /g' .temp | awk -F"  " '{print$2,$1}' > .temp2
      list_jar=$(sort -r .temp2 | awk '{print $3}')

      id_image=$(docker inspect --format="{{.Id}}" ${IMAGE_ETC42}-${ETC42_VERSION})

      echo ""
      echo "-------------------------------------------------------------------"
      echo "            etc42 versions available on :"
      echo ""
      echo "\033[31m- CeSAM server : ${CESAM_URL}\033[31m"
      echo "\033[32m- Localhost\033[0m"

      echo "-------------------------------------------------------------------"
      echo ""
      for jar in ${list_jar}
      do

          number_version=$(echo ${jar:8} | sed 's/\.jar//')
          if [ ${number_version} == ${ETC42_VERSION} ]; then
            echo "   \033[32m *\033[0m \033[31m${number_version}\033[0m"
          else
            echo "   \033[31m   ${number_version}\033[0m"

          fi


      done

      echo "-------------------------------------------------------------------"
      echo ""

      rm .temp
      rm .temp2


      #---------------------------------#
      ;;

    "-v"|"--version")
      #######################################
      # use: ./etc42_osx.sh -v OR --version #
      #######################################

      echo '- etc42_osx.sh version: ' ${SCRIPT_VERSION}

      #---------------------------------#
      ;;

    "-rm"|"--remove=")
      #######################################
      # use: ./etc42_osx.sh -rm OR --remove #
      #######################################

      version=$2

      list_image_version=$(docker images | awk '/^image_etc42-/{print $1}')
      present=$(echo $list_image_version| grep $version)

      if [ $? -eq 1 ]; then

        echo ''
        echo '------------------------------------'
        echo ''
        echo "You must delete an existing version"
        echo "    available on the localhost     "
        echo ''
        echo 'To see the available version(s):'
        echo ''
        echo "Please make ./etc42_osx.sh -i OR --infos"
        echo ''
        echo '------------------------------------'
        echo ''
        exit 0

      else

        function_delete_image $version

      fi
      #---------------------------------#
      ;;

    "-h"|"--help")
      ####################################
      # use: ./etc42_osx.sh -h OR --help #
      ####################################

      echo ""
      echo "========"
      echo "= HELP ="
      echo "========"
      echo ""
      echo "use : ./etc42_osx.sh [options]"
      echo ""
      echo ""
      echo "OPTIONS:"
      echo ""
      echo "-i, --infos                        give informations about versions available on cesam server"
      echo "                                   and give informations about etc42 image present on localhost"
      echo "-b version, --build version        build etc42 image with selected version"
      echo "--select version                   run etc42 with selected version number"
      echo "-u, --update                       update etc42 image version"
      echo "-rm version, --remove version      remove etc42 image with selected version"
      echo "-h, --help                         show this help message and exit"
      echo "-v, --version                      give etc42_osx.sh current version number"
      echo ""
      echo "====================="
      echo ""

    ;;

    *)
      #####################################
      # use: ./etc42_osx.sh OTHER COMMAND #
      #####################################

      echo ""
      echo "==============================="
      echo " error: unrecognized arguments "
      echo "==============================="
      echo ""
      echo "use : ./etc42_osx.sh [options]"
      echo ""
      echo ""
      echo "OPTIONS:"
      echo ""
      echo "-i, --infos                        give informations about versions available on cesam server"
      echo "                                   and give informations about etc42 image present on localhost"
      echo "-b version, --build version        build etc42 image with selected version"
      echo "--select version                   run etc42 with selected version number"
      echo "-u, --update                       update etc42 image version"
      echo "-rm version, --remove version      remove etc42 image with selected version"
      echo "-h, --help                         show this help message and exit"
      echo "-v, --version                      give etc42_osx.sh current version number"
      echo ""
      echo "====================="
      echo ""
    ;;

esac

#-----------------------------------------#
#-----------------------------------------#
#-----------------------------------------#
