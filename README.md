ETC42-docker-shell
======================

# Authors

CeSAM (Centre de donnéeS Astrophysique de Marseille) at LAM (Laboratoire d'Astrophysique de Marseille)

# About ETC42-docker-shell:

ETC42-docker-shell is a SHELL script use to launch [etc42](https://projets.lam.fr/projects/etc) with Docker.

This project allow to build a docker image with last etc42 jar version and launch it.

To launch ETC42-docker-shell there are two scripts:
- `etc42.sh` for linux users
- `etc42_osx.sh` for MAC-OS users

# Requirements

1. For Linux users:

  * You must have docker on your computer.(see [linux docker installation](https://docs.docker.com/install/))


2. For MAC-OS users:

  * You must have docker on your mac.(see [Mac-OS docker installation](https://docs.docker.com/docker-for-mac/install/))
  * You have to install XQuartz with command line `brew cask install xquartz` (see [XQuartz installation](https://www.xquartz.org/index.html) for MacPorts or others installations type).

  __*note*__: don't forget to select __Allow connections from network clients__ on XQuartz preferences (preferences -> security)

# Installation

1. Clone the etc42-docker-shell repository:
```
git clone git@gitlab.lam.fr:etc42/etc42-docker-shell.git
```

2. Launch the `etc42.sh` (or `etc42_osx.sh` for MAC-OS users) to create etc42 docker image, build and launch etc42 docker container :
```
./etc42.sh
```

# How to use ETC42

To see all options available for a good use of `etc42.sh` (or `etc42_osx.sh` for MAC-OS users) make:
```
./etc42.sh -h OR --help
```

here are the options available in detail:

1. Get etc42 script information, options available:
```
./etc42.sh -h OR --help
```
2. Get etc42 image version available on CeSAM server and on localhost:
```
./etc42.sh -i OR --infos
```
3. Build etc42 image with selected VERSION:
```
./etc42.sh -b VERSION OR --build VERSION
```
4. Select a specific etc42 image VERSION:
```
./etc42.sh --select VERSION
```
5. Update the current etc42 image:
```
./etc42.sh -u OR --update
```
6. Remove etc42 image with selected VERSION:
```
./etc42.sh -rm VERSION OR --remove VERSION
```
7. Give script etc42.sh current version number:
```
./etc42.sh -v OR --version
```

For the ETC42 user documentation see [ETC42 documentations](https://projets.lam.fr/projects/etc/wiki/Documentation)
